import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Guid } from 'guid-typescript';
import { MessageService } from 'primeng/api';
import { ImgDocumentDetails } from 'src/app/class/DTO/ImgDocument';
import { UserRequest } from 'src/app/class/request/UserRequest';
import { DocumentServiceService } from 'src/app/Services/document/document-service.service';
import { UserServiceService } from 'src/app/Services/user/user-service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [MessageService]
})
export class RegisterComponent implements OnInit {
  UserForm: FormGroup
  constructor(private userService: UserServiceService, private router: Router,
    private documentService: DocumentServiceService, private messageService: MessageService) { }

  userRequest: UserRequest = new UserRequest(null, null);
  message: string = "";
  time: number = 3000;

  ngOnInit(): void {
    this.userService.user = null;
    this.documentService.ListDocuments = new Map<Guid, ImgDocumentDetails>();
    this.UserForm = new FormGroup(
      {
        userID: new FormControl('', [Validators.required, Validators.email]),
        userName: new FormControl('')
      }
    )
    this.userService.OnRegisterUserResponseOK().subscribe(
      response => {
        this.userService.user = this.userRequest;
        this.messageService.add({ severity: 'success', summary: 'Success', detail: 'You have successfully registered', life: this.time });
        setTimeout(func => {this.userService.flages[2]=true, this.router.navigate(['/TableDocuments']) }, this.time)
      }
    )
    this.userService.OnUserExistsResponse().subscribe(
      Response => {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'user is exists pleas login', life: this.time });
        setTimeout(func => { this.router.navigate(['/Login']) }, this.time)
      }
    )
    this.userService.onEror().subscribe(
      message => console.log("Error", message)
    )
    this.userService.OnPropretyUserNullRespones().subscribe(
      massege => {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'the name or passworde is null', life: this.time });
      }
    )
  }

  onSubmit() {
    this.userRequest.userID = this.UserForm.value.userID;
    this.userRequest.userName = this.UserForm.value.userName;
    this.userService.Register(this.userRequest)
  }
}
