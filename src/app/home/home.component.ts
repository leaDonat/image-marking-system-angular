import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserServiceService } from '../Services/user/user-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  constructor(private userService: UserServiceService, private router: Router) { }

  ngOnInit(): void {
    if (!this.userService.user) {
      this.userService.user = null;
      setTimeout(func => {
        this.router.navigate(["Login"]);
      }, 5000)
    }
  }
}
