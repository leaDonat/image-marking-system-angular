import { Component, Input, OnInit } from '@angular/core';
import { Guid } from 'guid-typescript';
import { DocumentMarker } from '../class/DTO/MarkerDocument';
import { DrawingService } from '../Services/drawing/drawing.service';
import { MerkerServiseService } from '../Services/marker/merker-servise.service';

@Component({
  selector: 'app-style-marker',
  templateUrl: './style-marker.component.html',
  styleUrls: ['./style-marker.component.css']
})
export class StyleMarkerComponent implements OnInit {

  constructor(private markerService: MerkerServiseService, private drawingService: DrawingService) { }
  @Input()
  markerID: Guid;
  marker: DocumentMarker;
  ngOnInit(): void {
    this.marker = this.drawingService.markers.get(this.markerID)
  }
  saveChanges() {
    this.markerService.changeMarker(this.marker);
  }
}
