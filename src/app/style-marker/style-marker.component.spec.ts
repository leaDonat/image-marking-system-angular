import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StyleMarkerComponent } from './style-marker.component';

describe('StyleMarkerComponent', () => {
  let component: StyleMarkerComponent;
  let fixture: ComponentFixture<StyleMarkerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StyleMarkerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StyleMarkerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
