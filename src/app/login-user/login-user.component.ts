import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserServiceService } from '../Services/user/user-service.service';
import { Router } from '@angular/router';
import { UserRequest } from '../class/request/UserRequest';
import { DocumentServiceService } from '../Services/document/document-service.service';
import { User } from '../class/DTO/User';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-login-user',
  templateUrl: './login-user.component.html',
  styleUrls: ['./login-user.component.css'],
  providers: [MessageService]
})
export class LoginUserComponent implements OnInit {

  UserForm: FormGroup
  constructor(private userService: UserServiceService, private router: Router,
    private documentService: DocumentServiceService, private messageService: MessageService) { }
  userRequest: UserRequest = new UserRequest(null, null);
  message: string = "";
  time: number = 3000;
  userName: string;
  ngOnInit(): void {
    this.userService.user = new User();
    this.UserForm = new FormGroup(
      {
        userID: new FormControl('', [Validators.required, Validators.email]),
        userName: new FormControl('', [Validators.required,Validators.pattern("^[\u0590-\u05FF\a-zA-Z]+$")])
      }
    )
    this.userService.OnLoginUserResponseOK().subscribe(
      response => {
        this.userService.user = this.userRequest;
        this.documentService.fillMapDocuments(response.listDocuments);
        this.messageService.add({ severity: 'success', summary: 'Success', detail: 'You have logged in successfully', life: this.time });
        setTimeout(func => {this.userService.flages[2]=true,this.router.navigate(['/TableDocuments']), this.time})
      }
    )
    this.userService.OnUserNotExistsResponse().subscribe(
      response => {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'the user not exsist please register', life: this.time });
        setTimeout(func => this.router.navigate(['/Register']), this.time)
      }
    )
    this.userService.onEror().subscribe(
      message => console.log("Error", message)
    )
    this.userService.OnPropretyUserNullRespones().subscribe(
      massege => {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'the proprty is null', life: this.time });
      }
    )
    this.userService.OnSqlException().subscribe(
      massege => console.log(massege.error)
    )
  }

  onSubmit() {
    console.log(this.UserForm.value);
    this.userRequest.userID = this.UserForm.value.userID;
    this.userRequest.userName = this.UserForm.value.userName;
    this.userService.LoginUser(this.userRequest)
  }
}
