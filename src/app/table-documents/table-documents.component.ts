import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Guid } from 'guid-typescript';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { ImgDocumentDetails } from '../class/DTO/ImgDocument';
import { ShareDocument } from '../class/DTO/ShareDocument';
import { DocumentIDRequest } from '../class/request/DocumentRequest';
import { CreateDocumentService } from '../Services/document/create-document.service';
import { DocumentServiceService } from '../Services/document/document-service.service';
import { CreateShareService } from '../Services/share/share';
import { UserServiceService } from '../Services/user/user-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './table-documents.component.html',
  styles: [`
        :host ::ng-deep .p-dialog .product-image {
            width: 150px;
            margin: 0 auto 2rem auto;
            display: block;
        }
    `],
  styleUrls: ['./table-documents.component.css'],
  providers: [MessageService, ConfirmationService]

})
export class TableDocumentsComponent {

  fileCreateDialog: boolean;
  selectedDoc:any[];
  mapDoc: Map<Guid, ImgDocumentDetails> = this.documentServise.ListDocuments;
  selectedDocsDelete: Map<Guid, ImgDocumentDetails>=new Map<Guid, ImgDocumentDetails>();
  IdDoc: Guid;
  IdDocEye: Guid = null;
  imageUrl: string = null;
  createShareFlage: boolean = false;
  time: number = 3000;

  constructor(private router: Router, private documentServise: DocumentServiceService,
    private messageService: MessageService, private confirmationService: ConfirmationService,
    private userService: UserServiceService, private shareService: CreateShareService,
    private createdocServise: CreateDocumentService, private docServise: DocumentServiceService) { 
    this.userService.flages[1]=true;
    }

  ngOnInit() {
    this.documentServise.GetAllDocuments(this.userService.user.userID)
    this.documentServise.OnGetAllDocumentsUserResponseOK().subscribe(
      response => {
        this.documentServise.fillMapDocuments(response.imgsDoc)
        this.mapDoc = this.documentServise.ListDocuments;
      }
    )
  }

  ngAfterViewInit(): void {
    this.createdocServise.OnCreateDocumentResponseOK().subscribe(
      response => {
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'The document was successfully added', life: this.time });
        this.fileCreateDialog = false;
        this.documentServise.ListDocuments.set(response.imgDoc.docID, response.imgDoc);
      }
    )
    this.createdocServise.OnPropertyDocumentNull().subscribe(
      response => {
        {
          this.messageService.add({ severity: 'error', summary: 'fail', detail: 'property of document was null', life: this.time });
        }
      }
    )
    this.createdocServise.OnSqlException().subscribe(
      massege => console.log(massege.error)
    )

    this.documentServise.OnGetAllDocumentsUserResponseOK().subscribe(
      response => {
        this.documentServise.fillMapDocuments(response.imgsDoc)
        this.mapDoc = this.documentServise.ListDocuments;
      }
    )
    this.documentServise.OnRemoveDocumentResponseOK().subscribe(
      masage => {
        this.documentServise.GetAllDocuments(this.userService.user.userID)
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Document Deleted', life: this.time });
      }
    )
    this.documentServise.OnPropertyDocumentNull().subscribe(
      masage => {
        this.messageService.add({ severity: 'error', summary: 'fail', detail: 'property of document was null', life: this.time });
      }
    )
    this.documentServise.OnDocumentNotExists().subscribe(
      masage => {
        this.messageService.add({ severity: 'error', summary: 'fail', detail: 'document not Exists', life: this.time });
      }
    )
    this.documentServise.OnsqlException().subscribe(
      massege => console.log(massege.error)
    )
    this.shareService.OnCreateShareResponseOK().subscribe(
      response => {
        this.createShareFlage = false;
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'create share sucsess', life: this.time });
      }
    )
    this.shareService.OnEror().subscribe(m => {
      this.fileCreateDialog = false;
      console.log(m)
    });
    this.shareService.OnPropertyDocumentNull().subscribe(m => {
      this.fileCreateDialog = false;
      this.messageService.add({ severity: 'error', summary: 'fail', detail: 'property is null', life: this.time });
    });
    this.shareService.OnShareNotExists().subscribe(m => {
      this.fileCreateDialog = false;
      this.messageService.add({ severity: 'error', summary: 'fail', detail: 'Please check if there is no such sharing or alternatively if there is the userID you specified', life: this.time });
    });
    this.shareService.OnGetShareResponseOK().subscribe(
      m => {
        this.fileCreateDialog = false;
        console.log(m)
      }
    );
    this.shareService.OnSqlException().subscribe(
      massege => {
        this.fileCreateDialog = false;
        console.log(massege.error)
      }
    )
  }

  openNew() {
    this.fileCreateDialog = true;
  }
  openShare() {
    this.confirmationService.confirm({
      message: 'Are you sure you want to share the selected files?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.shareService.share = new ShareDocument();
        this.shareService.docsSelected = new Array<Guid>()    
        this.selectedDoc.forEach((  element)=>{
          this.shareService.docsSelected.push(element.key)
        })
        this.createShareFlage = true;
      }
    });

  }

  deleteSelectedFiles() {
console.log(this.selectedDoc);
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete the selected files?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.selectedDoc.forEach((  element)=>{
          console.log(element.value)
          this.documentServise.RemoveDocument(new DocumentIDRequest(element.key));
        })
      }
    });
  }

  ToDrawing(id: Guid) {
    this.router.navigate(['Drawing', id])
  }

  remove(docID: Guid) {
    this.IdDoc = docID;
    this.documentServise.RemoveDocument(new DocumentIDRequest(docID));
  }

  createShare(docID: Guid) {
    this.shareService.share = new ShareDocument();
    this.shareService.docsSelected = new Array<Guid>()
    this.shareService.docsSelected.push(docID)
    this.createShareFlage = true;
  }

  deleteFile(document: ImgDocumentDetails) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete ' + document.documentName + '?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.remove(document.docID)
      }
    });
  }

  hideDialog() {
    this.fileCreateDialog = false;
    this.createShareFlage=false;
  }

  statuseStringFunc(s: number) {
    switch (s) {
      case 1: return 'Director';
      case 2: return 'editing';
      case 3: return 'reading';  
    }
  }

  userIDString(id: string) {
    if (id == this.userService.user.userID)
      return 'me';
    return id;
  }

  openDetails(doc: ImgDocumentDetails) {
    this.imageUrl = doc.imageUrl;
    this.IdDocEye = doc.docID
  }

  closeDetails() {
    this.imageUrl = null;
    this.IdDocEye = null;
  }
  ngOnDestroy() {
    this.userService.flages[1]=false;
  }
}


