import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CreateShareService } from '../Services/share/share';

interface statuse {
  name: string,
  code: number
}

@Component({
  selector: 'app-create-share',
  templateUrl: './create-share.component.html',
  styleUrls: ['./create-share.component.css']
})

export class CreateShareComponent implements OnInit {
  statuses: statuse[] = [];
  createShareForem: FormGroup;

  constructor(private sahreService: CreateShareService) {
    this.statuses = [
      { name: 'Director', code: 1 },
      { name: 'editing', code: 2 },
      { name: 'reading', code: 3 }
    ]
  }

  ngOnInit(): void {
    this.createShareForem = new FormGroup(
      {
        userID: new FormControl('', [Validators.required, Validators.email]),
        statuse: new FormControl('')
      }
    )
  }

  onSubmit() {
    this.stuseString()
    this.sahreService.share.userID = this.createShareForem.value.userID;
    this.sahreService.share.statuse = this.createShareForem.value.statuse.code;
    this.sahreService.CreateShare();
  }

  stuseString() {
    switch (this.createShareForem.value.statuse) {
      case 'Director': this.sahreService.share.statuse = 1; break;
      case 'editing': this.sahreService.share.statuse = 2; break;
      case 'reading': this.sahreService.share.statuse = 3; break;
    }
  }
}