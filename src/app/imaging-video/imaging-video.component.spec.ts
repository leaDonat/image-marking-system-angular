import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImagingVideoComponent } from './imaging-video.component';

describe('ImagingVideoComponent', () => {
  let component: ImagingVideoComponent;
  let fixture: ComponentFixture<ImagingVideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImagingVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImagingVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
