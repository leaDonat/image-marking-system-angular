import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Guid } from 'guid-typescript';
import { ImgDocumentDetails } from '../class/DTO/ImgDocument';
import { DocumentMarker, DocumentMarkerDetails } from '../class/DTO/MarkerDocument';
import { Line } from '../class/line';
import { DocumentIDRequest } from '../class/request/DocumentRequest';
import { ShareDocumentRequest } from '../class/request/ShareDocumentRequest';
import { DocumentServiceService } from '../Services/document/document-service.service';
import { DrawingService } from '../Services/drawing/drawing.service';
import { MerkerServiseService } from '../Services/marker/merker-servise.service';
import { WebSocketService } from '../Services/webSocket/web-socket.service';
import { DomSanitizer } from '@angular/platform-browser';
import { PDFDocumentProxy } from 'ng2-pdf-viewer';
import { UserServiceService } from '../Services/user/user-service.service';
import { CreateShareService } from '../Services/share/share';
import { ShareDocument } from '../class/DTO/ShareDocument';
import { MenuItem, MessageService } from 'primeng/api';

@Component({
  selector: 'app-drawing',
  templateUrl: './drawing.component.html',
  styleUrls: ['./drawing.component.css'],
  providers: [MerkerServiseService, WebSocketService, DrawingService, MessageService]
})
export class DrawingComponent implements OnInit {
  @ViewChild("drawingCanvas") drawingCanvas: ElementRef
  @ViewChild("shapeCanvas") shapeCanvas: ElementRef
  @ViewChild("drawingFreeCanvas") drawingFreeCanvas: ElementRef
  @ViewChild("DowloadPDF") DowloadPDF: ElementRef

  marker: DocumentMarkerDetails;
  share: ShareDocumentRequest;
  doc: ImgDocumentDetails;
  flageImg: boolean;
  url: string;
  sizeDiv: number = 700;
  flageShare: boolean = false;
  message: string = "";
  selectMarkers: Map<Guid, boolean> = new Map<Guid, boolean>();
  time: number = 3000;
  option: MenuItem[];
  constructor(private router: Router, private drawingService: DrawingService
    , private markerService: MerkerServiseService, private docService: DocumentServiceService,
    private activ: ActivatedRoute, private sahreService: CreateShareService,
    private webService: WebSocketService, public sanitizer: DomSanitizer,
    private userService: UserServiceService, private messageService: MessageService) {
    this.userService.flages[0] = true;

  }

  ngOnInit(): void {
    this.marker = this.drawingService.marker;
    var idImg: Guid;
    this.activ.params.subscribe(suc => idImg = suc.index)
    this.drawingService.doc = this.docService.ListDocuments.get(idImg)
    this.markerService.GetMarkers(new DocumentIDRequest(this.drawingService.doc.docID));
    this.doc = this.drawingService.doc;
    this.flageImg = false;
    this.url = this.doc.imageUrl;
    this.share = new ShareDocumentRequest(this.userService.user.userID, this.drawingService.doc.docID)
    this.option = [
      {
        label: 'free draw', icon: 'pi pi-pencil', command: () => {
          this.ChangefreeDrow();
        }
      },
      {
        label: 'rectangel', icon: 'pi pi-mobile', command: () => {
          this.ChangeRectangel();
        }
      },
      {
        label: 'ellipse', icon: 'pi pi-circle-off', command: () => {
          this.ChangeEllipse();
        }
      },
      {
        label: 'Make a personal copy', icon: 'pi pi-copy', command: () => {
          this.copyFile();
        }
      },
      {
        label: 'create a share', icon: 'pi pi-share-alt', command: () => {
          this.createShare();
        }, disabled: this.doc.statuse != 1
      },
      {
        label: 'DowloadPDF', icon: 'pi pi-download', command: () => {
          this.dowloadPDFunc();
        }
      }
    ];
  }
  ngAfterViewInit() {
    this.drawingService.OnFinishCreateMarker().subscribe(
      marker =>
        this.markerService.CreateMarker(marker)
    )
    this.webService.OnchangeMarker().subscribe(
      response => {
        if (response.marker.docID == this.doc.docID) {
          this.drawingService.markers.delete(response.marker.docID),
            this.drawingService.markers.set(response.marker.markerID, response.marker),
            this.appendShapes();
        }
      })
    this.webService.OnRemoveMarker().subscribe(
      response => {
        if (response.marker.docID == this.doc.docID) {
          this.drawingService.markers.delete(response.marker.markerID),
            this.selectMarkers.delete(response.marker.markerID)
          this.appendShapes();
        }
      }
    )
    this.webService.OnCreateMarker().subscribe(
      response => {
        if (response.marker.docID == this.doc.docID) {
          this.drawingService.markers.set(response.marker.markerID, response.marker),
            this.appendShapes();
        }
      })
    this.markerService.OnSqlException().subscribe(
      massege => console.log(massege.error)
    )
    this.markerService.OnEror().subscribe(
      message => console.log(message.error)
    )

    this.markerService.OnPropretyUserNullRespones().subscribe(
      message => {
        this.messageService.add({ severity: 'Error', summary: 'fail', detail: 'document id is null please try again', life: this.time });
      }
    )
    this.markerService.OnMarkerNotExists().subscribe(
      message => {
        this.drawingService.markers = new Map<Guid, DocumentMarkerDetails>();
        this.webService.CreateWebSockt(new ShareDocumentRequest(this.userService.user.userID, this.doc.docID))
      }
    )
    this.markerService.OnGetMarkerResponse().subscribe(
      markers => {
        this.webService.CreateWebSockt(new ShareDocumentRequest(this.userService.user.userID, this.doc.docID))
        this.drawingService.fillMapMarkers(markers.markers), this.appendShapes()
      }
    )
    this.markerService.OnPropertyMarkerNull().subscribe(
      Response => {
        this.messageService.add({ severity: 'Error', summary: 'fail', detail: 'property is null please try again', life: this.time });
      }
    )
    this.markerService.OnCreateMarkerResponseOK().subscribe(
      marker => {
        this.drawingService.markers.set(marker.marker.markerID, marker.marker);
        this.displayShape(this.shapeCanvas.nativeElement, marker.marker);
      }
    )
    this.markerService.OnchangeMarkerResponseOK().subscribe(
      message => {
        console.log("all sharing see the new details of thise marker");
        this.clearCanvas(this.shapeCanvas.nativeElement);
        this.appendShapes();
      }
    )
    this.markerService.OnRemovemarkerResponseOK().subscribe(
      message => {
        this.drawingService.markers.delete(this.drawingService.marker.markerID);
        this.appendShapes();
      }
    )
    this.drawingService.OnShapeClick$().subscribe(
      response => {
        this.marker = this.drawingService.markers.get(response);
        this.marker.userID = this.userService.user.userID;
        this.selectMarkers.set(this.marker.markerID, true);
      }
    )
    this.sahreService.OnCreateShareResponseOK().subscribe(
      response => {
        this.flageShare = false;
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'the user in add', life: this.time });
        this.webService.CreateWebSockt(new ShareDocumentRequest(this.userService.user.userID, this.doc.docID))
      }
    )
    this.docService.OnDocumentNotExists().subscribe(
      s => { this.messageService.add({ severity: 'error', summary: 'fail', detail: 'document not existe', life: this.time }) },
      e => console.log(e)
    )
    this.docService.OnCopyDocResponseOK().subscribe(
      s => 
      {
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'the file copy successful', life: this.time }),
          this.doc.docID = s.docID;
          this.markerService.GetMarkers(new DocumentIDRequest(this.doc.docID));
      },
      e => console.log(e)
    )
    this.sahreService.OnEror().subscribe(m => { console.log(m) });
    this.sahreService.OnPropertyDocumentNull().subscribe(m => {
      this.messageService.add({ severity: 'error', summary: 'fail', detail: 'user id is null', life: this.time });
    });
    this.sahreService.OnShareNotExists().subscribe(m => {
      this.messageService.add({ severity: 'error', summary: 'fail', detail: 'This type of sharing already exists', life: this.time });
    });
    this.sahreService.OnGetShareResponseOK().subscribe(
    );
    this.sahreService.OnSqlException().subscribe(
      massege => console.log(massege.error)
    )
  }

  FreeDraw(drawingCanvas: ElementRef, line: Line) {
    var canvas = drawingCanvas.nativeElement
    var canvasShape = canvas.getBoundingClientRect()
    var ctx = canvas.getContext('2d')
    ctx.beginPath()
    ctx.moveTo(line.fromX, line.fromY)
    ctx.lineTo(line.toX, line.toY)
    ctx.stroke()
  }

  displayShape(canvas, marker: DocumentMarker) {
    if (marker != undefined) {
      var ctx = canvas.getContext('2d')
      ctx.beginPath()
      ctx.fillStyle = marker.backColor;
      ctx.strokeStyle = marker.foreColor;
      if (marker.markerType == "rec")
        ctx.rect(marker.markerLocationX, marker.markerLocationY, marker.radiusX * 2, marker.radiusY * 2)
      else
        if (marker.markerType == "ellipse")
          ctx.ellipse(marker.markerLocationX + marker.radiusX, marker.markerLocationY + marker.radiusY, marker.radiusX, marker.radiusY, 0, 0, 2 * Math.PI);
      ctx.fill();
      ctx.stroke()
      this.clearCanvas(this.drawingCanvas.nativeElement);
    }
  }

  appendShapes() {
    this.clearCanvas(this.shapeCanvas.nativeElement);
    this.drawingService.markers.forEach(element => {
      this.displayShape(this.shapeCanvas.nativeElement, element);
    });
  }

  ChangeRectangel() {
    this.drawingService.markerType = "rec"
  }

  ChangefreeDrow() {
    this.drawingService.markerType = "freeDrow"
  }

  ChangeEllipse() {
    this.drawingService.markerType = "ellipse"
  }

  clearCanvas(canvas) {
    var ctxF = canvas.getContext('2d');
    var rec = ctxF.clearRect(0, 0, canvas.width, canvas.height);
  }

  Close(id: Guid) {
    this.selectMarkers.delete(id);
  }

  remove(id: Guid) {
    this.drawingService.marker.userID = this.userService.user.userID;
    this.markerService.RemoveMarker(this.drawingService.marker);
    this.Close(id);
  }

  createShare() {
    this.sahreService.share = new ShareDocument();
    this.sahreService.docsSelected = new Array<Guid>()
    this.sahreService.docsSelected.push(this.doc.docID)
    this.flageShare = true;
  }

  Return() {
    this.router.navigate(['/Documents'])
  }

  callBackFn(event: PDFDocumentProxy) {
    var pages = event.numPages;
    this.shapeCanvas.nativeElement.width = this.sizeDiv
    this.shapeCanvas.nativeElement.height = 1133 * pages * (this.sizeDiv / 794)
    this.drawingFreeCanvas.nativeElement.width = this.sizeDiv
    this.drawingFreeCanvas.nativeElement.height = 1133 * pages * (this.sizeDiv / 794)
    if (this.doc.statuse == 3) {
      this.drawingCanvas.nativeElement.width = 0
      this.drawingCanvas.nativeElement.height = 0
      this.drawingService.Init2(this.shapeCanvas)
    }
    else {
      this.drawingCanvas.nativeElement.width = this.sizeDiv
      this.drawingCanvas.nativeElement.height = 1133 * pages * (this.sizeDiv / 794)
      this.drawingService.Init(this.drawingCanvas)
    }
    this.drawingService.OnfreeDraw$().subscribe(
      freeDrawObject => {
        if (this.drawingService.markerType == "freeDrow")
          this.FreeDraw(this.drawingFreeCanvas, freeDrawObject)
        else
          this.FreeDraw(this.drawingCanvas, freeDrawObject)
      }
    )
    this.drawingService.OnShapeDraw$().subscribe
      ((drawingObject: DocumentMarkerDetails) => this.displayShape(this.shapeCanvas.nativeElement, drawingObject))
    this.appendShapes()
  }

  dowloadPDFunc(): void {
    window.print();
  }

  ngOnDestroy() {
    this.userService.flages[0] = false;
    this.sahreService.closeWebSocket(new ShareDocumentRequest(this.userService.user.userID, this.doc.docID));
  }
  doUnload() {
    this.sahreService.closeWebSocket(new ShareDocumentRequest(this.userService.user.userID, this.doc.docID));
    return false;
  }
  hideDialog() {
    this.flageShare = false;
  }
  copyFile() {
    this.docService.CreateCopyFile(new DocumentIDRequest(this.doc.docID));
  }
}