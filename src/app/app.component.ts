import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DrawingService } from './Services/drawing/drawing.service';
import { UserServiceService } from './Services/user/user-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [DrawingService]
})
export class AppComponent {
  title = 'ProjectDev';
  AppFlags=this.userServise.flages;
  CurrentUser:string="";
  constructor(private router: Router,private userServise:UserServiceService) { }
  ngOnInit(){ }
  Home() {
    this.router.navigate([""])
  }
  Documents() {
      this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
      this.router.navigate(["TableDocuments"]));
  }
  ifLogin(){
    if(this.AppFlags[2])
    {
      this.CurrentUser=this.userServise.user.userName;
      return true;
    }
    return false;
  }
  
}
