import { DocumentMarker } from '../DTO/MarkerDocument';
import { Guid } from 'guid-typescript';

export class DocumentMarkeRequest extends DocumentMarker{
    constructor(public docID:Guid,public markerType:string,public markerLocationX:number
        ,public markerLocationY:number,public radiusX:number,public radiusY:number,
        public foreColor:string,public backColor:string,public userID:string,public myText:string)
        {
        super();
    }
}
export class DocumentMarkeIDRequest{
    constructor(public DocMarkerID?:Guid){
        
    }
}