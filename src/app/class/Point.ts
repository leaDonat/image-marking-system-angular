export class Point {
    constructor(public X: number, public Y: number) { }
    min(point: Point): Point {
        return (new Point(this.X > point.X ? point.X : this.X, this.Y > point.Y ? point.Y : this.Y))
    }
    max(point: Point): Point {
        return (new Point(this.X < point.X ? point.X : this.X, this.Y < point.Y ? point.Y : this.Y))
    }
}