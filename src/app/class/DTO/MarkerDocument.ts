import { Guid } from 'guid-typescript';
export class DocumentMarker  
{
   constructor(public docID?:Guid,public markerType?:string,public markerLocationX?:number
    ,public markerLocationY?:number,public radiusX?:number,public radiusY?:number,
    public foreColor?:string,public backColor?:string,public userID?:string,public myText?:string)
    {
        
    }
}
export class DocumentMarkerDetails extends DocumentMarker{
    constructor(public markerID?:Guid,public docID?:Guid,public markerType?:string,public markerLocationX?:number
        ,public markerLocationY?:number,public radiusX?:number,public radiusY?:number,
        public foreColor?:string,public backColor?:string,public userID?:string,public myText?:string){
        super(docID,markerType,markerLocationX,markerLocationY,radiusX,radiusY,foreColor,backColor,userID,myText);
    }
}  

