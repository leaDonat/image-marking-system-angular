import { Guid } from 'guid-typescript';

export class ImgDocument{
    constructor(public documentName?:string,public imageUrl?:string,public userID?:string,public dataString?:string){
    }
}
export class ImgDocumentDetails extends ImgDocument{
    constructor(public docID?:Guid,public documentName?:string,public imageUrl?:string,public userID?:string,public dataString?:string,public statuse?:number){
        super(documentName,imageUrl,userID,dataString);
    }
}
