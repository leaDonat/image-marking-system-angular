import { ShareDocument } from '../DTO/ShareDocument';
import { Guid } from 'guid-typescript';
import { DocumentMarker } from '../DTO/MarkerDocument';

export class CreateMarkerResponseOK extends DocumentMarker {
    constructor(public docID?:Guid,public markerType?:string,public markerLocationX?:number
        ,public markerLocationY?:number,public radiusX?:number,public radiusY?:number,
        public foreColor?:string,public backColor?:string,public userID?:string,public markerID?:Guid,public messageOK?: boolean) {
        super(docID,markerType,markerLocationX,markerLocationY,radiusX,radiusY,foreColor,backColor,userID);
        this.messageOK = true;
    }
}
export class GetMarkerResponse  {
    constructor(public messageOK?: boolean) {
        this.messageOK = true;
    }
}
export class RemoveMarkerResponseOK {
    constructor(public messageOK?: boolean) {
        this.messageOK = true;
    }
}
export class propertyMarkerNull  {
    constructor(public messageOK?: boolean) {
        this.messageOK = true;
    }
}
export class MarkerNotExists {
    constructor(public messageOK?: string) {
        
    }
}
