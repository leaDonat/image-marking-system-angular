import { ShareDocument } from '../DTO/ShareDocument';
import { Guid } from 'guid-typescript';

export class CreateShareResponseOK extends ShareDocument {
    constructor(public userID?:string,public docID?:Guid,public messageOK?: boolean) {
        super(userID,docID);
        this.messageOK = true;
    }
}
export class GetShareResponse  {
    constructor(public messageOK?: boolean) {
        this.messageOK = true;
    }
}
export class RemoveShareResponse {
    constructor(public messageOK?: boolean) {
        this.messageOK = true;
    }
}
export class ShareNotExists  {
    constructor(public messageOK?: boolean) {
        this.messageOK = true;
    }
}
export class PropertyShareNullResponse {
    constructor(public messageOK?: boolean) {
        this.messageOK = true;
    }
}
