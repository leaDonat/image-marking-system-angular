// export interface UserResponse{

import { User } from '../DTO/User';
import { ImgDocument } from '../DTO/ImgDocument';

export class UserExistsResponse extends User {
    constructor(public messageOK?: boolean) {
        super();
        this.messageOK = true;
    }
}


export class LoginUserResponseOK {
    public docs:Array<ImgDocument>=new Array<ImgDocument>();
    constructor(public messageOK?: boolean) {
        this.messageOK = true;

    }
}
export class RemoveUserResponse {
    constructor(public messageOK?: boolean) {
        this.messageOK = true;

    }
}
export class UserIDResponse {
    constructor(public userID?: string) {
    }
}
export class propretyUserNullResponse {
    constructor(public messageOK?: boolean) {
        this.messageOK = false;

    }
}
export class UserNotExistsResponse {
    constructor(public messageOK?: boolean) {
        this.messageOK = false;
    }
}
