import { ImgDocument, ImgDocumentDetails } from '../DTO/ImgDocument';
import { Guid } from 'guid-typescript';

export class GetDocumentResponse extends ImgDocumentDetails {
    constructor(public docID:Guid,public documentName?:string,public imageUrl?:string,public userID?:string,public messageOK?: boolean) {
        super(docID,documentName,imageUrl,userID);
        this.messageOK = true;
    }
}
export class DocumentResponseOK  {
    constructor(public messageOK?: boolean) {
        this.messageOK = true;
    }
}
export class CreateDocumentResponseOK  {
    constructor(public messageOK?: boolean) {
        this.messageOK = true;
    }
}
export class DocumentNotExists {
    constructor(public messageOK?: boolean) {
        this.messageOK = true;
    }
}
export class PropertyDocumentNull   {
    constructor(public messageOK?: boolean) {
        this.messageOK = true;
    }
}