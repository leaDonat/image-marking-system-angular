import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginUserComponent } from './login-user/login-user.component'
import { DrawingComponent } from './drawing/drawing.component';
import { RegisterComponent } from './Register/register/register.component';
import { CreateDocumentPdfComponent } from './create-document-pdf/create-document-pdf.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ImagingVideoComponent } from './imaging-video/imaging-video.component';
import { TableDocumentsComponent } from './table-documents/table-documents.component';

const routes: Routes = [
  { path: "Login", component: LoginUserComponent },
  { path: "Drawing/:index", component: DrawingComponent },
  { path: "Register", component: RegisterComponent },
  { path: "CreateDocumentPdf", component: CreateDocumentPdfComponent },
  { path: "", component: HomeComponent },
  { path: "Imaging Video", component: ImagingVideoComponent },
  { path: "About", component: AboutComponent },
  { path: "TableDocuments", component: TableDocumentsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
