import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'
import { DialogModule } from 'primeng/dialog';
import { LoginUserComponent } from './login-user/login-user.component';
import { FormsModule } from '@angular/forms';
import { DrawingComponent } from './drawing/drawing.component';
import { RegisterComponent } from './Register/register/register.component';
import { CreateDocumentPdfComponent } from './create-document-pdf/create-document-pdf.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ImagingVideoComponent } from './imaging-video/imaging-video.component';
import { TableModule } from 'primeng/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { TreeTableModule } from 'primeng/treetable';
import { TooltipModule } from 'primeng/tooltip';
import { RadioButtonModule } from 'primeng/radiobutton';
import { InputNumberModule } from 'primeng/inputnumber';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ToastModule } from 'primeng/toast';
import { InputTextModule } from 'primeng/inputtext';
import { FileUploadModule } from 'primeng/fileupload';
import { ToolbarModule } from 'primeng/toolbar';
import { RatingModule } from 'primeng/rating';
import { TableDocumentsComponent } from './table-documents/table-documents.component';
import { CreateShareComponent } from './create-share/create-share.component';
import { MultiSelectModule } from 'primeng/multiselect';
import { DropdownModule } from 'primeng/dropdown';
import { MenuModule } from 'primeng/menu';
import { ColorPickerModule } from 'primeng/colorpicker';
import { StyleMarkerComponent } from './style-marker/style-marker.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginUserComponent,
    DrawingComponent,
    RegisterComponent,
    CreateDocumentPdfComponent,
    HomeComponent,
    AboutComponent,
    ImagingVideoComponent,
    TableDocumentsComponent,
    CreateShareComponent,
    StyleMarkerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    PdfViewerModule,
    DialogModule,
    BrowserAnimationsModule,
    TableModule,
    ScrollingModule,
    ScrollPanelModule,
    TreeTableModule,
    InputNumberModule,
    ConfirmDialogModule,
    RadioButtonModule,
    TableModule,
    DialogModule,
    MultiSelectModule,
    ToastModule,
    InputTextModule,
    HttpClientModule,
    FileUploadModule,
    ToolbarModule,
    TooltipModule,
    RatingModule,
    FormsModule,
    RadioButtonModule,
    InputNumberModule,
    DropdownModule,
    MenuModule,
    ColorPickerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
