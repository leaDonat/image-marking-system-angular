import { ElementRef } from '@angular/core';
import { Guid } from 'guid-typescript';
import { fromEvent, Observable, Subject } from 'rxjs';
import { buffer, map, switchMap, takeUntil } from 'rxjs/operators';
import { ImgDocumentDetails } from 'src/app/class/DTO/ImgDocument';
import { DocumentMarkerDetails } from 'src/app/class/DTO/MarkerDocument';
import { Line } from 'src/app/class/line';
import { Point } from 'src/app/class/Point';
import { DocumentMarkeRequest } from 'src/app/class/request/DocumentMarkerRequest';
import { UserServiceService } from '../user/user-service.service';

export class DrawingService {
  constructor(private userServise: UserServiceService) { }
  markers = new Map<Guid, DocumentMarkerDetails>();
  doc: ImgDocumentDetails = new ImgDocumentDetails();
  markerType: string = "rec";
  marker: DocumentMarkerDetails = new DocumentMarkerDetails();
  drawingCanvas: ElementRef
  freeDrawSubject$ = new Subject<(Line)>();
  ShapeDrawSubject$ = new Subject<(DocumentMarkerDetails)>();     
  ShapeClick$ = new Subject<any>();
  FinishCreateMarker = new Subject<any>();

  fillMapMarkers(markersList: Array<DocumentMarkerDetails>) {
    this.markers = new Map<Guid, DocumentMarkerDetails>();
    for (let i = 0; i < markersList.length; i++)
      this.markers.set(markersList[i].markerID, markersList[i])
  }

  Init(drawingCanvas: ElementRef) {
    console.log("Init service", drawingCanvas)
    this.drawingCanvas = drawingCanvas
    var canvas = this.drawingCanvas.nativeElement
    var mouseUp$ = fromEvent(canvas, "mouseup")
    var mouseDown$ = fromEvent(canvas, "mousedown")
    var mouseMove$ = fromEvent(canvas, "mousemove")
    this.freeDrawEvents(mouseUp$, mouseMove$, mouseDown$)
  }

  Init2(shapeCanvase: ElementRef) {
    var markerClick$ = fromEvent(shapeCanvase.nativeElement, "click")
    markerClick$.subscribe((evt: MouseEvent) => { this.chackElement(evt, shapeCanvase) })
  }

  freeDrawEvents(mouseUp$, mouseMove$, mouseDown$) {
    var drawing$ = mouseDown$.pipe(
      switchMap(evt => mouseMove$.pipe(
        takeUntil(mouseUp$)
      )
      )
    )

    var obs$ = drawing$.pipe(
      map((evt: MouseEvent) => this.CreateLine(evt))
    )
    obs$.subscribe((freeDrawObject: { fromX: number, fromY: number, toX: number, toY: number }) =>
      this.freeDrawSubject$.next(freeDrawObject))


    var poly$ = this.freeDrawSubject$.pipe(
      buffer(mouseUp$)
    )
    var shape$ = poly$.pipe(
      map(poly => this.createMarker(poly))
    )
    shape$.subscribe(shape => {
      console.log("create shape")
    }
    )

    var markerClick$ = fromEvent(this.drawingCanvas.nativeElement, "click")
    markerClick$.subscribe((evt: MouseEvent) => { this.chackElement(evt, this.drawingCanvas) })
  }

  createMarker(poly) {
    if (this.markerType != "freeDrow") {
      var shapePoly = poly.map(elemObj => new Point(elemObj.toX, elemObj.toY))
      var min = new Point(500, 500)
      var max = new Point(0, 0)
      if (shapePoly.length > 0) {
        min = shapePoly.reduce((acc, pt) => acc.min(pt))
        max = shapePoly.reduce((acc, pt) => acc.max(pt))
        var radiusx = (max.X - min.X) / 2;
        var radiusy = (max.Y - min.Y) / 2;
        if (radiusx > 1 && radiusy > 1) {
          var marker = new DocumentMarkeRequest(this.doc.docID, this.markerType, Math.floor(min.X),
            Math.floor(min.Y), Math.floor(radiusx), Math.floor(radiusy), "black", "transparent", this.userServise.user.userID, "");
          this.FinishCreateMarker.next(marker);
        }
      }
    }
  }

  CreateLine(evt: MouseEvent) {
    var rect = this.drawingCanvas.nativeElement.getBoundingClientRect()
    var toX = evt.clientX - rect.left
    var toY = evt.clientY - rect.top
    var fromX = toX - evt.movementX
    var fromY = toY - evt.movementY
    return new Line(fromX, fromY, toX, toY);
  }

  chackElement(event: MouseEvent, canvase: ElementRef) {
    var rect = canvase.nativeElement.getBoundingClientRect()
    var x = event.clientX - rect.left;
    var y = event.clientY - rect.top;
    this.marker=null;
    this.markers.forEach(element => {
      if (y > element.markerLocationY && y < element.markerLocationY + element.radiusX * 2
        && x > element.markerLocationX && x < element.markerLocationX + element.radiusX * 2) {
        this.marker = element;
      }
    })
    if (this.marker!= null)
      this.ShapeClick$.next(this.marker.markerID);
  }

  OnfreeDraw$(): Observable<(Line)> {
    return this.freeDrawSubject$;
  }
  OnShapeDraw$(): Observable<(DocumentMarkerDetails)> {
    return this.ShapeDrawSubject$;
  }
  OnShapeClick$(): Observable<(Guid)> {
    return this.ShapeClick$;
  }
  OnFinishCreateMarker(): Observable<any> {
    return this.FinishCreateMarker;
  }
}
