import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RemoteCommUserService {

  constructor(private http: HttpClient) { }
  LoginUser(value: any): Observable<any> {
    return this.http.post('services/api/User/Login', value);
  }
  Register(user: any): Observable<any> {
    return this.http.post('services/api/User/CreateUser', user)
  }
}
