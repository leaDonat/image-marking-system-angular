import { TestBed } from '@angular/core/testing';

import { RemoteCommUserService } from './remote-comm-user.service';

describe('RemoteCommUserService', () => {
  let service: RemoteCommUserService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RemoteCommUserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
