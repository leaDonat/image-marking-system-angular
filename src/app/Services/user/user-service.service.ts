import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators'
import { User } from 'src/app/class/DTO/User';
import { RemoteCommUserService } from './remote-comm-user.service';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  responseSubjects: { [responseID: string]: Subject<any> } =
    {
      LoginUserResponseOK: new Subject<any>(),
      UserExistsResponse: new Subject<any>(),
      PropretyUserNullRespones: new Subject<any>(),
      UserNotExistsResponse: new Subject<any>(),
      RegisterUserResponseOK: new Subject<any>(),
      SqlException: new Subject<any>()
    }
  errorSubject = new Subject<any>();
  user: User;
  flages:Array<boolean>=new Array<boolean>();
  constructor(private remoteComm: RemoteCommUserService) {
    this.flages[0]=false;
    this.flages[1]=false;
    this.flages[2]=false;
   }

  LoginUser(value: any) {
    var obs = this.remoteComm.LoginUser(value);
    var obsPipe = obs.pipe(
      map(response => [response, this.responseSubjects[response.responseType]])
    )
    obsPipe.subscribe(
      ([response, responseSubject]) => responseSubject.next(response),
      error => this.onEror().next(error)
    )
  }
  Register(value: any) {
    var obs = this.remoteComm.Register(value);
    var obsPipe = obs.pipe(
      map(response => [response, this.responseSubjects[response.responseType]])
    )
    obsPipe.subscribe(
      ([response, responseSubject]) => responseSubject.next(response),
      error => this.onEror().next(error)
    )
  }
  onEror(): Subject<any> {
    return this.errorSubject;
  }
  OnLoginUserResponseOK(): Subject<any> {
    return this.responseSubjects.LoginUserResponseOK;
  }
  OnUserExistsResponse(): Subject<any> {
    return this.responseSubjects.UserExistsResponse;
  }
  OnPropretyUserNullRespones(): Subject<any> {
    return this.responseSubjects.PropretyUserNullRespones;
  }
  OnUserNotExistsResponse(): Subject<any> {
    return this.responseSubjects.UserNotExistsResponse;
  }
  OnRegisterUserResponseOK(): Subject<any> {
    return this.responseSubjects.RegisterUserResponseOK;
  }
  OnSqlException(): Subject<any> {
    return this.responseSubjects.SqlException;
  }
}
