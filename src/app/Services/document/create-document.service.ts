import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { DocumentRequest } from 'src/app/class/request/DocumentRequest';
import { Subject } from 'rxjs';
import { RemoteCommDocumentService } from './remote-comm-document.service';

@Injectable({
  providedIn: 'root'
})


export class CreateDocumentService {

  responseSubjects: { [responseID: string]: Subject<any> } =
    {
      CreateDocumentResponseOK: new Subject<any>(),
      PropertyDocumentNull: new Subject<any>(),
      UserNotExsist: new Subject<any>(),
      SqlException: new Subject<any>(),
    }
  errorSubject = new Subject<any>();
  constructor(private remotS: RemoteCommDocumentService) { }
  createDocument(documentR: DocumentRequest) {
    var obs = this.remotS.createDocument(documentR);
    var obsPipe = obs.pipe(
      map(response => [response, this.responseSubjects[response.responseType]])
    )
    obsPipe.subscribe(
      ([response, responseSubject]) => { responseSubject.next(response) },
      err => this.OnEror().next(err)
    )
  }
  OnEror(): Subject<any> {
    return this.errorSubject;
  }
  OnCreateDocumentResponseOK(): Subject<any> {
    return this.responseSubjects.CreateDocumentResponseOK;
  }
  OnPropertyDocumentNull(): Subject<any> {
    return this.responseSubjects.PropertyDocumentNull;
  }
  OnUserNotExsist(): Subject<any> {
    return this.responseSubjects.UserNotExsist;
  }
  OnSqlException(): Subject<any> {
    return this.responseSubjects.SqlException;
  }

}
