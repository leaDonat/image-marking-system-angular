import { TestBed } from '@angular/core/testing';

import { RemoteCommDocumentService } from './remote-comm-document.service';

describe('RemoteCommDocumentService', () => {
  let service: RemoteCommDocumentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RemoteCommDocumentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
