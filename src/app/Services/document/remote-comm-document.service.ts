import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DocumentIDRequest, DocumentRequest } from 'src/app/class/request/DocumentRequest';
import { UserIDRequest } from 'src/app/class/request/UserRequest';

@Injectable({
  providedIn: 'root'
})
export class RemoteCommDocumentService {

  constructor(private http: HttpClient) { }

  createDocument(documentR: DocumentRequest): Observable<any> {
    return this.http.post('services/api/Document/CreateDocument', documentR);
  }
  CreateCopyFile(docID:DocumentIDRequest):Observable<any>{
    return this.http.post('services/api/Document/CreateCopyFile', docID)
  }
  RemoveDocument(documentR: DocumentIDRequest): Observable<any> {
    return this.http.post('services/api/Document/RemoveDocument', documentR);
  }
  GetAllDocuments(uderID: UserIDRequest): Observable<any> {
    return this.http.post('services/api/Document/GetDocumentsForUser', uderID);
  }

}
