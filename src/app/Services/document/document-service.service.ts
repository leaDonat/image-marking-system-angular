import { Injectable } from '@angular/core';
import { ImgDocumentDetails } from '../../class/DTO/ImgDocument';
import { Subject } from 'rxjs';
import { DocumentIDRequest, DocumentRequest } from 'src/app/class/request/DocumentRequest';
import { map } from 'rxjs/operators';
import { RemoteCommDocumentService } from './remote-comm-document.service';
import { Guid } from 'guid-typescript';
import { UserIDRequest } from 'src/app/class/request/UserRequest';

@Injectable({
  providedIn: 'root'
})

export class DocumentServiceService {
  public ListDocuments: Map<Guid, ImgDocumentDetails> = new Map<Guid, ImgDocumentDetails>();

  responseSubjects: { [responseID: string]: Subject<any> } =
    {
      CreateDocumentResponseOK: new Subject<any>(),
      PropertyDocumentNull: new Subject<any>(),
      UserNotExsist: new Subject<any>(),
      RemoveDocumentResponseOK: new Subject<any>(),
      DocumentNotExists: new Subject<any>(),
      CopyDocResponseOK:new Subject<any>(),
      sqlException: new Subject<any>(),
      GetAllDocumentsUserResponseOK: new Subject<any>()
    }
  errorSubject = new Subject<any>();

  constructor(private remotS: RemoteCommDocumentService) { }

  createDocument(documentR: DocumentRequest) {
    var obs = this.remotS.createDocument(documentR);
    var obsPipe = obs.pipe(
      map(response => [response, this.responseSubjects[response.responseType]])
    )
    obsPipe.subscribe(
      ([response, responseSubject]) => { responseSubject.next(response) },
      err => this.OnEror().next(err)
    )
  }

  CreateCopyFile(docID: DocumentIDRequest) {
    var obs = this.remotS.CreateCopyFile(docID);
    var obsPipe = obs.pipe(
      map(response => [response, this.responseSubjects[response.responseType]])
    )
    obsPipe.subscribe(
      ([response, responseSubject]) => { responseSubject.next(response) },
      err => this.OnEror().next(err)
    )
  }

  RemoveDocument(documentR: DocumentIDRequest) {
    var obs = this.remotS.RemoveDocument(documentR);
    var obsPipe = obs.pipe(
      map(response => [response, this.responseSubjects[response.responseType]])
    )
    obsPipe.subscribe(
      ([response, responseSubject]) => { responseSubject.next(response) },
      err => this.OnEror().next(err)
    )
  }
  GetAllDocuments(userID: string) {  
    var obs = this.remotS.GetAllDocuments(new UserIDRequest(userID));
    var obsPipe = obs.pipe(
      map(response => [response, this.responseSubjects[response.responseType]])
    )
    obsPipe.subscribe(
      ([response, responseSubject]) => { responseSubject.next(response) },
      err => this.OnEror().next(err)
    )
  }
  fillMapDocuments(DocList: Array<ImgDocumentDetails>) {
    this.ListDocuments=new Map<Guid,ImgDocumentDetails>()
    for (let i = 0; i < DocList.length; i++)
      this.ListDocuments.set(DocList[i].docID, DocList[i])
  }
  OnEror(): Subject<any> {
    return this.errorSubject;
  }
  OnCreateDocumentResponseOK(): Subject<any> {
    return this.responseSubjects.CreateDocumentResponseOK;
  }
  OnPropertyDocumentNull(): Subject<any> {
    return this.responseSubjects.PropertyDocumentNull;
  }
  OnUserNotExsist(): Subject<any> {
    return this.responseSubjects.UserNotExsist;
  }
  OnRemoveDocumentResponseOK(): Subject<any> {
    return this.responseSubjects.RemoveDocumentResponseOK;
  }
  OnDocumentNotExists(): Subject<any> {
    return this.responseSubjects.DocumentNotExists;
  }
  OnCopyDocResponseOK():Subject<any>{
    return this.responseSubjects.copyDocResponseOK;
  }
  OnsqlException(): Subject<any> {
    return this.responseSubjects.sqlException;
  }

  OnGetAllDocumentsUserResponseOK(): Subject<any> {
    return this.responseSubjects.GetAllDocumentsUserResponseOK;
  }
}
