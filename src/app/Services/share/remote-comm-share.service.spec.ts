import { TestBed } from '@angular/core/testing';

import { RemoteCommShareService } from './remote-comm-share.service';

describe('RemoteCommShareService', () => {
  let service: RemoteCommShareService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RemoteCommShareService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
