import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ShareDocumentRequest } from 'src/app/class/request/ShareDocumentRequest';

@Injectable({
  providedIn: 'root'
})
export class RemoteCommShareService {

  constructor(private http: HttpClient) { }
  CreateShare(doc: ShareDocumentRequest): Observable<any> {
    return this.http.post('services/api/SharedDocumen/CreateShare', doc);
  }
  GetShare(doc: ShareDocumentRequest): Observable<any> {
    return this.http.post('services/api/SharedDocumen/GetShare', doc);
  }
  closeWebSocket(share: ShareDocumentRequest) {
    return this.http.post('services/api/SharedDocumen/CloseWebSocket', share);
  }
}
