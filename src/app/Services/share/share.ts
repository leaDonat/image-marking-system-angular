import { Injectable } from '@angular/core';
import { Guid } from 'guid-typescript';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { ShareDocument } from 'src/app/class/DTO/ShareDocument';
import { ShareDocumentRequest } from 'src/app/class/request/ShareDocumentRequest';
import { RemoteCommShareService } from './remote-comm-share.service';

@Injectable({
  providedIn: 'root'
})

export class CreateShareService {
  share: ShareDocument;
  docsSelected: Array<Guid> = new Array<Guid>();
  constructor(private remotS: RemoteCommShareService) { }
  responseSubjects: { [responseID: string]: Subject<any> } =
    {
      CreateShareResponseOK: new Subject<any>(),
      ShareNotExists: new Subject<any>(),
      PropertyShareNullResponse: new Subject<any>(),
      GetShareResponseOK: new Subject<any>(),
      SqlException: new Subject<any>()
    }
  errorSubject = new Subject<any>();

  CreateShare() {
    this.docsSelected.forEach(e => {
      this.createShare2(e)
    })
  }
  createShare2(docID: Guid) {
    var obs = this.remotS.CreateShare(new ShareDocumentRequest(this.share.userID, docID, this.share.statuse));
    var obsPipe = obs.pipe(
      map(response => [response, this.responseSubjects[response.responseType]])
    )
    obsPipe.subscribe(
      ([response, responseSubject]) => { responseSubject.next(response) },
      err => this.OnEror().next(err)
    )
  }
  GetShare(doc: ShareDocumentRequest) {
    var obs = this.remotS.GetShare(doc);
    var obsPipe = obs.pipe(
      map(response => [response, this.responseSubjects[response.responseType]])
    )
    obsPipe.subscribe(
      ([response, responseSubject]) => { responseSubject.next(response) },
      err => this.errorSubject.next(err)
    )
  }
  closeWebSocket(share: ShareDocumentRequest) {
    var obs = this.remotS.closeWebSocket(share).subscribe(
    );
  }

  OnEror(): Subject<any> {
    return this.errorSubject;
  }
  OnCreateShareResponseOK(): Subject<any> {
    return this.responseSubjects.CreateShareResponseOK;
  }
  OnPropertyDocumentNull(): Subject<any> {
    return this.responseSubjects.PropertyShareNullResponse;
  }
  OnShareNotExists(): Subject<any> {
    return this.responseSubjects.ShareNotExists;
  }
  OnGetShareResponseOK(): Subject<any> {
    return this.responseSubjects.GetShareResponseOK;
  }
  OnSqlException(): Subject<any> {
    return this.responseSubjects.SqlException;
  }
}

