import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { webSocket } from 'rxjs/webSocket';
import { ShareDocumentRequest } from 'src/app/class/request/ShareDocumentRequest';

@Injectable()
export class WebSocketService {

  constructor() { }
  responseSubjects: { [responseID: string]: Subject<any> } =
    {
      changeMarker: new Subject<any>(),
      CreateMarker: new Subject<any>(),
      RemoveMarker: new Subject<any>(),
      errorSubject: new Subject<any>()
    }



  CreateWebSockt(share: ShareDocumentRequest) {
    var obs = webSocket(
      { url: "wss://localhost:44387/ws?userID=" + share.userID + "&docID=" + share.docID, deserializer: msg => msg })
    var e = obs.pipe(
      map((response: any) => [JSON.parse(response.data), this.responseSubjects[JSON.parse(response.data).responseType]])
    )
    e.subscribe(
      ([response, responseSubject]) => { responseSubject.next(response), console.log("response   " + response) },
      err => this.responseSubjects.errorSubject.next(err)

    )
  }
  OnEror(): Subject<any> {
    return this.responseSubjects.errorSubject;
  }
  OnRemoveMarker(): Subject<any> {
    return this.responseSubjects.RemoveMarker;
  }
  OnCreateMarker(): Subject<any> {
    return this.responseSubjects.CreateMarker;
  }
  OnchangeMarker(): Subject<any> {
    return this.responseSubjects.changeMarker;
  }

}