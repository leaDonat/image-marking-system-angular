import { TestBed } from '@angular/core/testing';

import { MerkerServiseService } from './merker-servise.service';

describe('MerkerServiseService', () => {
  let service: MerkerServiseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MerkerServiseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
