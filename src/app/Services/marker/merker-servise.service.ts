import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { DocumentMarkerDetails } from 'src/app/class/DTO/MarkerDocument';
import { DocumentMarkeRequest, DocumentMarkeIDRequest } from 'src/app/class/request/DocumentMarkerRequest';
import { DocumentIDRequest } from 'src/app/class/request/DocumentRequest';
import { RemoteCommMarkerService } from './remote-comm-marker.service';


@Injectable()
export class MerkerServiseService {

  responseSubjects: { [responseID: string]: Subject<any> } =
    {  
      GetMarkerResponse: new Subject<any>(),
      CreateMarkerResponseOK: new Subject<any>(),
      MarkerNotExists: new Subject<any>(),
      PropretyUserNullRespones: new Subject<any>(),
      changeMarkerResponseOK: new Subject<any>(),
      RemoveMarkerResponseOK: new Subject<any>(),
      SqlException: new Subject<any>(),
      PropertyMarkerNull: new Subject<any>()
    }
  errorSubject = new Subject<any>();

  constructor(private remotS: RemoteCommMarkerService) { }

  GetMarkers(docID: DocumentIDRequest) {
    var obs = this.remotS.GetMarkers(docID);
    var obsPipe = obs.pipe(
      map(response => [response, this.responseSubjects[response.responseType]])
    )
    obsPipe.subscribe(
      ([response, responseSubject]) => { responseSubject.next(response) },
      err => this.OnEror().next(err)
    )
  }

  CreateMarker(marker: DocumentMarkeRequest) {
    var obs = this.remotS.createMarker(marker);
    var obsPipe = obs.pipe(
      map(response => [response, this.responseSubjects[response.responseType]])
    )
    obsPipe.subscribe(
      ([response, responseSubject]) => responseSubject.next(response),
      err => this.OnEror().next(err)
    )
  }
  changeMarker(marker: DocumentMarkerDetails) {
    var obs = this.remotS.changeMarker(marker);
    var obsPipe = obs.pipe(
      map(response => [response, this.responseSubjects[response.responseType]])
    )
    obsPipe.subscribe(
      ([response, responseSubject]) => responseSubject.next(response),
      err => this.OnEror().next(err)
    )
  }
  RemoveMarker(marker: DocumentMarkerDetails) {
    var obs = this.remotS.RemoveMarker(marker);
    var obsPipe = obs.pipe(
      map(response => [response, this.responseSubjects[response.responseType]])
    )
    obsPipe.subscribe(
      ([response, responseSubject]) => responseSubject.next(response),
      err => this.OnEror().next(err)
    )
  }
  OnEror(): Subject<any> {
    return this.errorSubject;
  }
  OnGetMarkerResponse(): Subject<any> {
    return this.responseSubjects.GetMarkerResponse;
  }
  OnCreateMarkerResponseOK(): Subject<any> {
    return this.responseSubjects.CreateMarkerResponseOK;
  }
  OnMarkerNotExists(): Subject<any> {
    return this.responseSubjects.MarkerNotExists;
  }
  OnPropretyUserNullRespones(): Subject<any> {
    return this.responseSubjects.PropretyUserNullRespones;
  }
  OnchangeMarkerResponseOK(): Subject<any> {
    return this.responseSubjects.changeMarkerResponseOK;
  }
  OnRemovemarkerResponseOK(): Subject<any> {
    return this.responseSubjects.RemoveMarkerResponseOK;
  }
  OnSqlException(): Subject<any> {
    return this.responseSubjects.SqlException;
  }
  OnPropertyMarkerNull(): Subject<any> {
    return this.responseSubjects.PropertyMarkerNull;
  }
}






