import { TestBed } from '@angular/core/testing';

import { RemoteCommMarkerService } from './remote-comm-marker.service';

describe('RemoteCommMarkerService', () => {
  let service: RemoteCommMarkerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RemoteCommMarkerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
