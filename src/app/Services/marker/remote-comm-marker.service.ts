import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DocumentMarkerDetails } from 'src/app/class/DTO/MarkerDocument';
import { DocumentMarkeRequest } from 'src/app/class/request/DocumentMarkerRequest';
import { DocumentIDRequest } from 'src/app/class/request/DocumentRequest';

@Injectable({
  providedIn: 'root'
})
export class RemoteCommMarkerService {

  constructor(private http: HttpClient) { }
  GetMarkers(docID: DocumentIDRequest): Observable<any> {
    return this.http.post('services/api/DocumentMarker/GetMarkers', docID);
  }
  createMarker(marker: DocumentMarkeRequest): Observable<any> {
    return this.http.post('services/api/DocumentMarker/CreateMarker', marker);
  }
  changeMarker(marker: DocumentMarkerDetails): Observable<any> {
    return this.http.post('services/api/DocumentMarker/changeMarker', marker);
  }
  RemoveMarker(marker: DocumentMarkerDetails): Observable<any> {
    return this.http.post('services/api/DocumentMarker/RemoveMarker', marker);
  }
}
