import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { User } from '../class/DTO/User';
import { DocumentRequest } from '../class/request/DocumentRequest';
import { CreateDocumentService } from '../Services/document/create-document.service';
import { UserServiceService } from '../Services/user/user-service.service';

@Component({
  selector: 'app-create-document-pdf',
  templateUrl: './create-document-pdf.component.html',
  styleUrls: ['./create-document-pdf.component.css'],
})
export class CreateDocumentPdfComponent implements OnInit {
  DocumentForm: FormGroup;
  url: SafeResourceUrl;
  cardImageBase64: string;
  document: DocumentRequest = new DocumentRequest();
  message: string = "";
  flageMessage: boolean = false;
  time: number = 3000;
  constructor(private createdocServise: CreateDocumentService, private userService: UserServiceService, private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.DocumentForm = new FormGroup({
      DocumentName: new FormControl('', [Validators.required]),
      DocumentUrl: new FormControl('', [Validators.required])
    })
  }

  onSubmit() {
    this.document.documentName = this.DocumentForm.value.DocumentName;
    this.document.imageUrl = this.cardImageBase64;
    var user: User = this.userService.user;
    this.document.userID = user.userID;
    this.createdocServise.createDocument(this.document);
  }

  fileChangeEvent(fileInput: any) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      console.log("fileInput: " + fileInput.target.files[0]);
      const reader = new FileReader();
      reader.readAsDataURL(fileInput.target.files[0]);
      reader.onload = (e: any) => {
        const image = new Image();
        image.src = e.target.result;
        this.cardImageBase64 = e.target.result;
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.cardImageBase64);
      };
    }
  }

  removeIagme() {
    this.cardImageBase64 = null;
  }

}


