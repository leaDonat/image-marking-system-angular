import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateDocumentPdfComponent } from './create-document-pdf.component';

describe('CreateDocumentPdfComponent', () => {
  let component: CreateDocumentPdfComponent;
  let fixture: ComponentFixture<CreateDocumentPdfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateDocumentPdfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDocumentPdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
